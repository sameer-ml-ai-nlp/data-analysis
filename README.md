# Data Analysis

1. df-chunk_size  
   Explains usage of "chunk_size" property of DataFrame when we need to process large datasets and it's not possible to load the entire dataset into memory.  

4. Data Manipulation with Pandas  
   Pandas is built on NumPy and Matplotlib:  
   NumPy -> Provides multi-dimentional array objects for easy data manipulation which Pandas uses to store data.  
   Matplotlib -> It has powerful data visualisation capabilities that pandas takes advantage of.  

   